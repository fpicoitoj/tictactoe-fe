import React, { Component } from "react";
import { connect } from "react-redux";
import Score from "./../../components/score";
import Board from "./../../components/board";
import Winner from "./../../components/winner";
import {
  createGame,
  fetchGame,
  makeMove,
  rematch
} from "./../../store/actions";
import "./style.css";

class Game extends Component {
  constructor(props) {
    super(props);

    this.handleOnBoardButtonClick = this.handleOnBoardButtonClick.bind(this);
    this.handleOnNewGameClick = this.handleOnNewGameClick.bind(this);
    this.handleOnRematchClick = this.handleOnRematchClick.bind(this);
  }

  // ### child components handlers
  handleOnBoardButtonClick(position) {
    this.props.makeMove(this.props.match.params.id, position);
  }

  async handleOnNewGameClick() {
    const game = await this.props.createGame();
    this.props.history.push(`/game/${game._id}`);
  }

  handleOnRematchClick() {
    this.props.rematch(this.props.match.params.id);
  }

  // ### component functions
  async fetchGame(id) {
    const game = await this.props.fetchGame(id);
    // if 500 means we couldnt get game so 404 it
    if (game.status === 500) this.props.history.push("/404");
  }

  // ### component life cycle
  componentDidMount() {
    this.fetchGame(this.props.match.params.id);
  }

  componentWillReceiveProps(newProps) {
    // if the game/:id changed then we need to fetch new game
    if (newProps.match.params.id !== this.props.match.params.id) {
      this.fetchGame(newProps.match.params.id);
    }
  }

  render() {
    if (this.props.game.board === null) {
      return (
        <div className="game">
          <span className="game--loading">
            <i className="fas fa-spinner fa-spin fa-3x" />
          </span>
        </div>
      );
    }

    return (
      <div className="game">
        <Score
          isXPlaying={this.props.game.isXPlaying}
          xWins={this.props.game.xWins}
          oWins={this.props.game.oWins}
          draws={this.props.game.draws}
        />
        <Board
          board={this.props.game.board}
          isPlayerOneTurn={this.props.isPlayerOneTurn}
          onBoardButtonClick={this.handleOnBoardButtonClick}
        />

        {/* If there is a winner, show it up as a modal */}
        {this.props.game.winner && (
          <Winner
            winner={this.props.game.winner}
            onNewGame={this.handleOnNewGameClick}
            onRematch={this.handleOnRematchClick}
          />
        )}
      </div>
    );
  }
}

function mapStateToProps({ gameReducer }) {
  return {
    game: gameReducer
  };
}

export default connect(mapStateToProps, {
  createGame,
  fetchGame,
  makeMove,
  rematch
})(Game);
