import React from "react";
import { Link } from "react-router-dom";
import "./style.css";

const NotFound = () => {
  return (
    <div className="notfound">
      <img src={require("./../../assets/error-404.svg")} alt="404" />
      <h1>Oops not found!</h1>
      <Link to="/">Go back</Link>
    </div>
  );
};

export default NotFound;
