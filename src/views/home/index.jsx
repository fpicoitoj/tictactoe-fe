import React, { Component } from "react";
import { connect } from "react-redux";
import { createGame, fetchTotalNumberOfGames } from "./../../store/actions";
import Button from "./../../components/button";
import "./style.css";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      totalNumberOfGames: 0
    };

    this.handleCreateNewGameClick = this.handleCreateNewGameClick.bind(this);
  }

  async handleCreateNewGameClick() {
    const game = await this.props.createGame();
    this.props.history.push(`game/${game._id}`);
  }

  componentDidMount() {
    this.props.fetchTotalNumberOfGames(totalNumberOfGames => {
      this.setState({
        totalNumberOfGames
      });
    });
  }

  render() {
    return (
      <div className="home">
        <img src={require("./../../assets/tic-tac-toe.svg")} alt="logo" />
        <h2>{this.state.totalNumberOfGames} games have been played so far</h2>
        <Button onClick={this.handleCreateNewGameClick} addClass="huge">
          <span>Play new game</span>
          <i className="fas fa-chess-knight" />
        </Button>
      </div>
    );
  }
}

export default connect(null, {
  fetchTotalNumberOfGames,
  createGame
})(Home);
