import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import Game from "./views/game";
import Home from "./views/home";
import NotFound from "./views/notFound";
import "./style.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Link to="/">
            <h1 className="App-title">Tic Tac Toe!</h1>
          </Link>
        </header>

        <div className="container">
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/game/:id" component={Game} />
            <Route path="*" component={NotFound} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
