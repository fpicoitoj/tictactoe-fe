/**
 * File to have all the constants so we're less prone to mistakes
 */
export const X = "X";
export const O = "O";
