import API from "./../index";

/**
 * Game API helper which has all the necessary routes
 */
class GameAPI extends API {
  constructor() {
    super();
    this.uri = "game";
  }

  async getTotalNumberOfGames() {
    const result = await this.apiGet(`${this.uri}/get-total`);
    return result;
  }

  async get(id) {
    const result = await this.apiGet(`${this.uri}/${id}`);
    return result;
  }

  async create(id) {
    const result = await this.apiPost(`${this.uri}`);
    return result;
  }

  async update(id, payload) {
    const result = await this.apiPut(`${this.uri}/${id}`, payload);
    return result;
  }

  async makeMove(id, position) {
    const result = await this.apiPost(`${this.uri}/${id}/make-move`, {
      position
    });
    return result;
  }

  async rematch(id) {
    const result = await this.apiPost(`${this.uri}/${id}/rematch`);
    return result;
  }
}

const api = new GameAPI();
export default api;
