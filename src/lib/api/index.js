import axios from "axios";

/**
 * This is very simply a wrapper for Axios
 * All the APIs in the modules folder will extend this
 * and call upon its functions
 */
class API {
  constructor() {
    this.http = axios.create({
      baseURL: process.env.REACT_APP_API_URL
    });

    this.http.interceptors.response.use(r => r, this._handleError);
  }

  async _request(req) {
    try {
      const { data } = await this.http(req);
      if (typeof data === "object") data.status = 200;
      return data;
    } catch (err) {
      debugger;
      return {
        status: 500,
        message: err.response.data.message
      };
    }
  }

  apiGet(url, config = null) {
    return this._request({
      method: "get",
      url,
      ...config
    });
  }

  apiPost(url, payload, config = null) {
    return this._request({
      method: "post",
      url,
      data: payload,
      ...config
    });
  }

  apiPut(url, payload, config = null) {
    return this._request({
      method: "put",
      url,
      data: payload,
      ...config
    });
  }

  apiPatch(url, payload, config = null) {
    return this._request({
      method: "patch",
      url,
      data: payload,
      ...config
    });
  }

  apiDelete(url, config = null) {
    return this._request({
      method: "delete",
      url,
      ...config
    });
  }
}

export default API;
