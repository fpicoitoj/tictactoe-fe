/**
 * Typically I always create "ing" verbs for loading states
 * but given this is a small scope project they are overkill
 * hence we aren't doing really anything with it
 * but in other cases we always fire them before an async task
 */
export const GAME_REMATCHING = "GAME_REMATCHING";
export const GAME_MAKE_MOVE = "GAME_MAKE_MOVE";
export const GAME_FETCHING = "GAME_FETCHING";
export const GAME_CREATING = "GAME_CREATING";
export const GAME_REMATCH = "GAME_REMATCH";
export const GAME_CREATE = "GAME_CREATE";
export const GAME_FETCH = "GAME_FETCH";
