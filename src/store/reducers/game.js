import {
  GAME_MAKE_MOVE,
  GAME_REMATCH,
  GAME_CREATE,
  GAME_FETCH
} from "../types";

/**
 * Typically created to show the structure (guidance)
 * and init variables
 */
const INITIAL_STATE = {
  board: null,
  isXPlaying: true,
  xWins: 0,
  oWins: 0,
  draws: 0,
  winner: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GAME_FETCH:
      return {
        ...INITIAL_STATE,
        ...action.payload
      };

    case GAME_REMATCH:
      return {
        ...state,
        ...action.payload
      };

    case GAME_CREATE:
      return {
        ...INITIAL_STATE,
        ...action.payload
      };

    case GAME_MAKE_MOVE:
      return {
        ...state,
        ...action.payload
      };

    default:
      return state;
  }
};
