import {
  GAME_REMATCHING,
  GAME_MAKE_MOVE,
  GAME_FETCHING,
  GAME_CREATING,
  GAME_REMATCH,
  GAME_FETCH
} from "./../types";
import gameAPI from "./../../lib/api/modules/game";

/**
 * Fetch total number of games
 */
export const fetchTotalNumberOfGames = callback => async dispatch => {
  const payload = await gameAPI.getTotalNumberOfGames();
  if (callback) callback(payload);
};

/**
 * Fetch a specific game
 *
 * @param {ObjectId} id
 * @param {function} callback
 */
export const fetchGame = id => {
  return async dispatch => {
    dispatch({
      type: GAME_FETCHING
    });

    const payload = await gameAPI.get(id);

    // if fetched game correctly dispatch a fetch
    if (payload.status === 200) {
      dispatch({
        type: GAME_FETCH,
        payload
      });
    }

    return payload;
  };
};

/**
 * Create a new game
 *
 * @param {function} callback
 */
export const createGame = () => {
  return async dispatch => {
    dispatch({
      type: GAME_CREATING
    });

    const payload = await gameAPI.create();

    return payload;
  };
};

/**
 * Make a move on the board
 *
 * @param {ObjectId} id
 * @param {number} pos
 * @param {function} callback
 */
export const makeMove = (id, pos) => async dispatch => {
  const payload = await gameAPI.makeMove(id, pos);

  dispatch({
    type: GAME_MAKE_MOVE,
    payload
  });
};

/**
 * Play again!
 *
 * @param {ObjectId} id
 */
export const rematch = id => {
  return async dispatch => {
    dispatch({
      type: GAME_REMATCHING
    });

    const payload = await gameAPI.rematch(id);
    dispatch({
      type: GAME_REMATCH,
      payload
    });
  };
};
