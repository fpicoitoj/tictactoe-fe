import React, { Component } from "react";
import ReactModal from "react-modal";
import "./style.css";

/**
 * Base modal
 * refer to  https://github.com/reactjs/react-modal
 */
class Modal extends Component {
  constructor(props) {
    super(props);

    this.handleOnClose = this.handleOnClose.bind(this);

    this.state = {
      isOpen: false
    };
  }

  componentWillMount() {
    this.setState({ isOpen: this.props.isOpen });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isOpen !== this.state.isOpen) {
      this.setState({ isOpen: nextProps.isOpen });
    }
  }

  handleOnClose() {
    this.setState({ isOpen: false });

    if (this.props.onClose) this.props.onClose();
  }

  render() {
    const { icon, title, children } = this.props;

    return (
      <ReactModal
        isOpen={this.state.isOpen}
        shouldReturnFocusAfterClose={false}
        ariaHideApp={false}
        className={{
          base: "modal",
          afterOpen: "modal__after-open",
          beforeClose: "modal__before-close"
        }}
        overlayClassName={{
          base: "modal__overlay",
          afterOpen: "modal__overlay__after-open",
          beforeClose: "modal__overlay__before-close"
        }}
      >
        <div className="header">
          <div className="header-info">
            <i className={`${icon} fa-2x`} />
            <span>{title}</span>
          </div>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 50 100"
            preserveAspectRatio="none"
          >
            <polygon fill="white" points="0,100 100,0 100,100" />
          </svg>
          {this.props.canClose && (
            <div className="close">
              <button onClick={this.handleOnClose}>
                <i className="fa fa-times" />
              </button>
            </div>
          )}
        </div>
        <div className="content">{children}</div>
      </ReactModal>
    );
  }
}

export default Modal;
