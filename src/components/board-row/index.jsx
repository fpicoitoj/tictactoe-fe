import React from "react";
import "./style.css";

/**
 * Simple board row just to align children
 */
const BoardRow = ({ children }) => {
  return <div className="board__row">{children}</div>;
};

export default BoardRow;
