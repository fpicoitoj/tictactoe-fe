import React from "react";
import { X, O } from "./../../lib/consts";
import "./style.css";

/**
 * Perhaps the name could be tile but it's just where the user can click
 * to make a move
 */
const BoardButton = ({ value, position, addClass, onClick }) => {
  const renderIcon = () => {
    switch (value) {
      case X:
        return <i className="fas fa-times" />;
      case O:
        return <i className="far fa-circle" />;
      default:
        return null;
    }
  };

  return (
    <button
      type="button"
      className={`board__button ${addClass || ""} ${
        value === X ? "is-x" : "is-o"
      }`}
      onClick={() => onClick(position)}
    >
      {renderIcon()}
    </button>
  );
};

export default BoardButton;
