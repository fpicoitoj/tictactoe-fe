import React from "react";
import "./style.css";

/**
 * Displays score statistics and dyes based on who is playing
 */
const Score = ({ isXPlaying, xWins, oWins, draws }) => {
  return (
    <div className="score">
      <div
        className={`score__player score__player--x ${
          !isXPlaying ? "score__player--inactive" : ""
        }`}
      >
        <i className="fas fa-times fa-2x" />
        <span>{xWins}</span>
      </div>
      <div className="score__player score__player--draw">
        <i className="fas fa-question fa-2x" />
        <span>{draws}</span>
      </div>
      <div
        className={`score__player score__player--o ${
          isXPlaying ? "score__player--inactive" : ""
        }`}
      >
        <i className="far fa-circle fa-2x" />
        <span>{oWins}</span>
      </div>
    </div>
  );
};

export default Score;
