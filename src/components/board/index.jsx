import React from "react";
import BoardRow from "../board-row";
import BoardButton from "../board-button";
import "./style.css";

/**
 * The most basic board component
 * Simply render the board for all it's 9 positions
 * and apply the button effect click on each one
 * - - -
 * this is stricly for 3x3 :)
 */
const Board = ({ board, isPlayerOneTurn, onBoardButtonClick }) => {
  const renderBoardButtons = startingIndex => {
    let returnArray = [];
    for (let i = startingIndex; i < startingIndex + 3; i++) {
      returnArray.push(
        <BoardButton
          value={board[i]}
          position={i}
          key={i}
          onClick={onBoardButtonClick}
        />
      );
    }
    return returnArray;
  };

  return (
    <div className="board">
      <BoardRow>{renderBoardButtons(0)}</BoardRow>
      <BoardRow>{renderBoardButtons(3)}</BoardRow>
      <BoardRow>{renderBoardButtons(6)}</BoardRow>
    </div>
  );
};

export default Board;
