import React from "react";
import Modal from "./../modal";
import Button from "./../button";
import { X } from "./../../lib/consts";
import "./style.css";

/**
 * Winner modal with calculation on title and icon
 */
const Winner = ({ icon, winner, onRematch, onNewGame }) => {
  let winnerObj;

  if (winner === "-1") {
    winnerObj = {
      title: "Draw!",
      icon: "fas fa-flag"
    };
  } else {
    winnerObj = {
      title: `${winner} wins!`,
      icon: `fas fa-trophy ${winner === X ? "is-x" : "is-o"}`
    };
  }

  return (
    <Modal
      isOpen={true}
      title={winnerObj.title}
      icon={winnerObj.icon}
      canClose={false}
    >
      <div className="modal__winner">
        <div className="modal__winner__options">
          <Button addClass={"link"} onClick={onNewGame}>
            New Game
          </Button>
          <Button addClass={"action"} onClick={onRematch}>
            Play Again
          </Button>
        </div>
      </div>
    </Modal>
  );
};

export default Winner;
