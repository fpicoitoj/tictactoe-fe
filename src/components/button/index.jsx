import React from "react";
import "./style.css";

/**
 * Base button
 */
const Button = ({ children, addClass, onClick }) => {
  return (
    <button type="button" className={`btn ${addClass || ""}`} onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
